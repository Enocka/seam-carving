import edu.princeton.cs.algs4.Picture;
import edu.princeton.cs.algs4.IndexMinPQ;
import edu.princeton.cs.algs4.Topological;
// import edu.princeton.cs.algs4.Vector;
import java.awt.Color;
import java.lang.Math;

public class SeamCarver {
    private final Picture picture;
    private final Picture originalPicture;
    private int[][] edgeTo;
    private double[][] energy;
    private double[][] energyTo;
    private boolean[][] energyCalculated;
    private int[][] pixels;
    private IndexMinPQ<Double> pq;
    private boolean VERTICAL;
   // private Vector<Color> colorVector;

    // create a seam carver object based on the given picture
    public SeamCarver(Picture picture){
        this.picture = picture;
        originalPicture = new Picture(picture);
        pixels = new int[width()][height()];
        VERTICAL = true;
    }

    // current picture
    public Picture picture(){
        return picture;
    }

    // width of current picture
    public     int width(){
        return picture.width();
    }

    // height of current picture
    public     int height(){
        return picture.height();
    }

    // energy of pixel at column x and row y
    public  double energy(int x, int y){
        if (energyCalculated[x][y])
            return energy[x][y];
        if (x == 0 || x == (width() - 1) || 
            y == 0 || y == (height() - 1)){
            pixels[x][y] = picture.getRGB(x, y);
            energyCalculated[x][y] = true;
            energyTo[x][y] = 1000;
            energy[x][y] = 1000;
            return 1000;
        }
        pixels[x][y] = picture.getRGB(x, y);
        // getRGB():  it doesn't return a color object
        Color colorMinus1 = picture.get(x-1, y);
        Color colorPlus1 = picture.get(x+1, y);
        int Rx = colorPlus1.getRed() - colorMinus1.getRed();
        int Gx = colorPlus1.getGreen() - colorMinus1.getGreen();
        int Bx = colorPlus1.getBlue() - colorMinus1.getBlue();
        int sumOfXSqures = (Rx ^ 2) + (Gx ^ 2) + (Bx ^ 2);
        colorMinus1 = picture.get(x, y-1);
        colorPlus1 = picture.get(x, y+1);
        Rx = colorPlus1.getRed() - colorMinus1.getRed();
        Gx = colorPlus1.getGreen() - colorMinus1.getGreen();
        Bx = colorPlus1.getBlue() - colorMinus1.getBlue();
        int sumOfYSqures = (Rx ^ 2) + (Gx ^ 2) + (Bx ^ 2);
        double pixelEnergy = Math.sqrt(sumOfXSqures + sumOfYSqures);
        energyCalculated[x][y] = true;
        energy[x][y] = pixelEnergy;
        return pixelEnergy;
    }

    // sequence of indices for horizontal seam
    public   int[] findHorizontalSeam(){
        // transposePicture();
        return findVerticalSeam();
    }
    
    private int flatten(int x, int y){
        return (y * width()) + (x - 1);
    }
    
    private void initializeDistances(){
        for (int i = 0; i < width(); i++){
            energyTo[i][0] = 1000;
        }
        for (int i = 0; i < width(); i++){
            for (int j = 1; j < height(); j++){
                energyTo[i][j] = Double.POSITIVE_INFINITY;
            }
        }
    }

    // sequence of indices for vertical seam
    public   int[] findVerticalSeam(){
        int width = width();
        int height = height();
        energy = new double[width][height];
        energyTo = new double[width][height];
        edgeTo = new int[width][height];
        int bottomDirect, bottomLeft, bottomRight, current;
        int shortestSeamSource, shortestSeamEnd;
        pq = new IndexMinPQ<Double>(width * height);
        initializeDistances();
        for (int i = 0; i < width(); i++){
            pq.insert(i, 1000.0);
        }
        while(!pq.isEmpty()){
            int v = pq.delMin();
            int vX = xValue(v);
            int vY = yValue(v);
            energy(vX, vY);
            energy(vX-1, vY+1);
            energy(vX, vY+1);
            energy(vX+1, vY+1);
            relax(flatten(vX, vY), flatten(vX, vY+1));
            
            relax(flatten(vX, vY), flatten(vX-1, vY+1));
            
            relax(flatten(vX, vY), flatten(vX+1, vY+1));
        }
        double shortestEnergyPath = Double.POSITIVE_INFINITY;
        int endPoint = height * width - 1;
        for (int i = 0; i < width; i++){
            if (energyTo[i][height - 1] < shortestEnergyPath)
                endPoint = flatten(i, height-1);
        }
        int[] verticalSeam = new int[height];
        int currentHLevel = height;
        while(currentHLevel >= 0){
            verticalSeam[--currentHLevel] = xValue(endPoint);
            endPoint = edgeTo[xValue(endPoint)][yValue(endPoint)];
        }
        return verticalSeam;
    }
    
    private void relax(int from, int current){
        if (energyTo[xValue(current)][yValue(current)] > 
            energyTo[xValue(from)][yValue(from)] + energy[xValue(current)][yValue(current)])
        {
            energyTo[xValue(current)][yValue(current)] = energyTo[xValue(from)][yValue(from)] + energy[xValue(current)][yValue(current)];
            edgeTo[xValue(current)][yValue(current)] = from;
            if (pq.contains(current))
                pq.decreaseKey(current, energyTo[xValue(current)][yValue(current)]);
            else pq.insert(current, energyTo[xValue(current)][yValue(current)]);
        };
    }
    
    private int xValue(int flattened){
        return (flattened+1)%width();
    }
    
    private int yValue(int flattened){
        if (flattened < width()) return 0;
        return (flattened+1)/width();
    }

    // remove horizontal seam from current picture
    public    void removeHorizontalSeam(int[] seam){
        
    }

    // remove vertical seam from current picture
    public    void removeVerticalSeam(int[] seam){

    }
}